if ( ! Modernizr.objectfit ) {
    $('.objectfit-container').each(function () {
        var $container = $(this),
        imgUrl = $container.find('img').prop('src');
        if (imgUrl) {
            $container
            .css('backgroundImage', 'url(' + imgUrl + ')')
            if ($container.hasClass('objectfit-container-cover')) {
                $container.addClass('objectfit-cover');
            } else if ($container.hasClass('objectfit-container-contain')) {
                $container.addClass('objectfit-contain');
            }
        }
    });
}

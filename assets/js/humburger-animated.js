(function() {
	/* In animations (to close icon) */
	var beginAC = 80,
	    endAC = 320,
	    beginB = 80,
	    endB = 320;

	function inAC(s) {
	    s.draw('80% - 240', '80%', 0.3, {
	        delay: 0.1,
	        callback: function() {
	            inAC2(s)
	        }
	    });
	}

	function inAC2(s) {
	    s.draw('100% - 545', '100% - 305', 0.6, {
	        easing: ease.ease('elastic-out', 1, 0.3)
	    });
	}

	function inB(s) {
	    s.draw(beginB - 60, endB + 60, 0.1, {
	        callback: function() {
	            inB2(s)
	        }
	    });
	}

	function inB2(s) {
	    s.draw(beginB + 120, endB - 120, 0.3, {
	        easing: ease.ease('bounce-out', 1, 0.3)
	    });
	}

	/* Out animations (to burger icon) */

	function outAC(s) {
	    s.draw('90% - 240', '90%', 0.1, {
	        easing: ease.ease('elastic-in', 1, 0.3),
	        callback: function() {
	            outAC2(s)
	        }
	    });
	}

	function outAC2(s) {
	    s.draw('20% - 240', '20%', 0.3, {
	        callback: function() {
	            outAC3(s)
	        }
	    });
	}

	function outAC3(s) {
	    s.draw(beginAC, endAC, 0.7, {
	        easing: ease.ease('elastic-out', 1, 0.3)
	    });
	}

	function outB(s) {
	    s.draw(beginB, endB, 0.7, {
	        delay: 0.1,
	        easing: ease.ease('elastic-out', 2, 0.4)
	    });
	}

	/* Awesome burger default */

	function addScale(m) {
		m.className = 'menu-icon-wrapper scaled';
	}

	function removeScale(m) {
		m.className = 'menu-icon-wrapper';
	}


	var pathA = document.getElementById('pathA'),
		pathB = document.getElementById('pathB'),
		pathC = document.getElementById('pathC'),
		segmentD = new Segment(pathA, beginAC, endAC),
		segmentE = new Segment(pathB, beginB, endB),
		segmentF = new Segment(pathC, beginAC, endAC),
		trigger = document.getElementById('menu-icon-trigger'),
		toCloseIcon = true,
		wrapper = document.getElementById('menu-icon-wrapper');

	wrapper.style.visibility = 'visible';

	trigger.onclick = function() {
		addScale(wrapper);
		if (toCloseIcon) {
			inAC(segmentD);
			inB(segmentE);
			inAC(segmentF);
			$('body').removeClass('panel-closed').addClass('panel-opened');
			openPanel();
		} else {
			addScale(wrapper);
	        outAC(segmentD);
			outB(segmentE);
			outAC(segmentF);
			$('body').removeClass('panel-opened').addClass('panel-closed');
			closePanel();
		}
		toCloseIcon = !toCloseIcon;
		setTimeout(function() {
			removeScale(wrapper)
		}, 300);

	};
})();

// Slide Panel

var leftPanel = $('aside.left'),
	rigthPanel = $('aside.right');

	function openPanel() {
		jQuery.easing.def = "string";

		$(leftPanel).removeClass('left-close', 300, 'easeOutExpo');
		$(rigthPanel).removeClass('right-open', 300, 'easeInExpo');

		$(panelScroll).perfectScrollbar("update");

	}

	function closePanel() {
		jQuery.easing.def = "string";

		$(leftPanel).addClass('left-close', 300, 'easeOutExpo');
		$(rigthPanel).addClass('right-open', 300, 'easeInExpo');

		$(panelScroll).perfectScrollbar("update");

	}
